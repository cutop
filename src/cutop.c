#include "cutop.h"




int main (int argc, char **argv) {
	void (*xfunc)(CUdevice dev) = NULL;        
	
	int loop = 0;
	int opt;
	
	
	
	while((opt = getopt(argc, argv, "hl:tmvxdro")) != -1) {
		switch(opt) {
			case 'l':
				loop = atoi(optarg);
				break;
			case 'm':
				xfunc = &query_memory;
				break;
			case 'x':
				xfunc = &drv_leak_mem;
				break;
			case 'd':
				xfunc = &tst_drvapi;
				break;
			case 'r':
				xfunc = &tst_runtime;
				break;
			case 'o':
				xfunc = &tst_opencl;
				break;
			case 't':
				xfunc = &tst_threads;
				break;
			case 'v':
				printf("This is cutop - version %s\n", CUTOP_VERSION);
				exit(1);
			default:
				show_usage();
		}
	}
	
	if(xfunc != NULL) {
		printf("cutop version %s - (C) 2010 <adrian.ulrich@cscs.ch>\n", CUTOP_VERSION);
		REPEAT_FUNC:
		xfunc(0);
		
		if(loop > 0) {
			sleep(loop);
			goto REPEAT_FUNC;
		}
		
	}
	else {
		show_usage();
	}
	
	
	
	exit(0);
}

void show_usage() {
	printf("Usage: cutop [-l loop_sec] [..opts..]\n");
	
	printf("   -d      : run sample using the driver api (same as -m)\n");
	printf("   -r      : run sample using the runtime api\n");
	printf("   -o      : run sample using the OpenCL api (not implemented yet)\n");
	printf("   -m      : query memory info\n");
	printf("   -x      : leak memory and crash\n");
	printf("   -v      : print version and exit.\n");
	printf("\n");
	exit(1);
}


/*********************************************************************
 * uses driver api - leaks memory: createctx without freeing them
 *********************************************************************/
void drv_leak_mem(CUdevice unused) {
	CUdevice dev;
	CUcontext ctx;
	int devcount, i;
	size_t mem_free;
	cuOK( cuInit(0) );
	cuOK( cuDeviceGetCount(&devcount) );
	
	printf("# leaking memory on %d device(s)\n",devcount);
	
	for(;;) {
		for(i=0;i<devcount;i++) {
			cuOK( cuDeviceGet(&dev,i) );
			cuOK( cuCtxCreate(&ctx,0,dev) );
			cuOK( cuMemGetInfo(&mem_free, NULL) );
			printf("device=%2d, free_bytes=%4lu\n", i, mem_free);
		}
	}
	
	
}


/*********************************************************************
 * display memory information and name
 *********************************************************************/
void query_memory(CUdevice unused) {
	
	pid_t mypid;
	struct utsname myself;
	unsigned int i;                        /* loop var           */
	int devcount;                          /* device count       */
	CUdevice dev;                          /* cuda device handle */
	CUcontext ctx;                         /* context handle     */
	size_t mem_free, mem_total;            /* memory info        */
	char devname[NV_DEVNAME_SIZE] = {0};   /* device name        */
	
	cuOK( cuInit(0) );
	cuOK( cuDeviceGetCount(&devcount) );
	
	
	mypid = getpid();
	uname(&myself);
	
	printf("# found %d cuda-aware devices\n",devcount);
	
	for(i=0;i<devcount;i++) {
		
		cuOK( cuDeviceGet(&dev, i) );
		cuOK( cuDeviceGetName(devname, NV_DEVNAME_SIZE, dev) );
		cuOK( cuCtxCreate(&ctx, 0, dev) );
		cuOK( cuMemGetInfo(&mem_free, &mem_total) );
		
		printf("host=%s pid=%d device=%2d, name=%20s, mem_total=%4luMiB, mem_free=%4luMiB\n", myself.nodename, mypid, i, devname, mem_total/1024/1024, mem_free/1024/1024);
		
		cuOK( cuCtxDetach(ctx) );
	}
}

/*********************************************************************
 * display device properties
 *********************************************************************/
void query_devprops(CUdevice unused) {
	CUdevice devcount, i;
	struct cudaDeviceProp devprop;
	
	cuOK(cudaGetDeviceCount(&devcount));
	
	printf("# runtime API reports %d cuda-aware devices\n", devcount);
	
	for(i=0;i<devcount;i++) {
		cuOK( cudaGetDeviceProperties(&devprop,i) );
		printf("device=%2d, name=%20s, clock_rate=%d, compute_mode=%d\n", i, devprop.name, devprop.clockRate, devprop.computeMode);
	}
	
}

void tst_threads(CUdevice x) {
	#define NTHREADS 5
	pthread_t threads[NTHREADS];
	int thread_args[NTHREADS];
	int r, i;
	printf(">> CREATING THREADS RIGHT NOW\n");
	for (i=0; i<NTHREADS; ++i) {
		thread_args[i] = i;
		r = pthread_create(&threads[i], NULL, _thcode, (void *) &thread_args[i]);
	}
	
	for (i=0; i<NTHREADS; ++i) {
		r = pthread_join(threads[i], NULL);
	}
	exit(0);
}

void *_thcode(void *argument)
{
	int thread_id;
	
	thread_id = *((int *) argument);
	
	cudaSetDevice(0);
	printf("This is thread %d // %d\n", thread_id, pthread_self());
	
	return 0;
}

void tst_runtime(CUdevice x) {
	query_devprops(x);
}

void tst_drvapi(CUdevice x) {
	query_memory(x);
}

void tst_opencl(CUdevice x) {
	xdie("Not implemented %d\n",x);
}


/*********************************************************************
 * runs a simple cublas test
 *********************************************************************/
void cublas_test(CUdevice nvdev) {
	
}


/*********************************************************************
 * print a backtrace on error
 *********************************************************************/
void cuOK(CUresult retcode) {
	if(retcode != CUDA_SUCCESS) {
		fprintf(stderr, "==== CALL FAILED WITH RETURN VALUE %d, BACKTRACE: ====\n",retcode);
		xbtrace();
		exit(1);
	}
}



/*********************************************************************
 * print a backtrace
 *********************************************************************/
void xbtrace (void) {
	void *array[10];
	char **smbl;
	size_t size, i;
	
	size = backtrace(array,10);
	smbl = backtrace_symbols(array,size);
	for(i=0;i<size;i++) {
		printf("%lu -> %s\n", i, smbl[i]);
	}
	free(smbl);
}

/*********************************************************************
 * die!
 *********************************************************************/
void xdie(const char *fmt, long int code) {
	fprintf(stderr, fmt, code);
	exit(1);
}

