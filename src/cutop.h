#include <cuda.h>
#include <driver_types.h>
#include <cuda_runtime_api.h>
#include <cublas.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/utsname.h>
#include <unistd.h>
#include <execinfo.h>
#include <pthread.h>


#define NV_DEVNAME_SIZE 256


#define CUTOP_VERSION "0.1"



void xdie(const char *fmt, long  int code);
void xbtrace (void);
void show_usage();
void cuOK(CUresult );



void query_memory(CUdevice);
void drv_leak_mem(CUdevice);
void query_devinfo(CUdevice);
void query_devprops(CUdevice);



void tst_runtime(CUdevice  );  // jump to a function using the runtime api
void tst_drvapi(CUdevice   );  // jump to a function using the driver api
void tst_opencl(CUdevice   );  // jump to a function using the OpenCL api
void tst_threads(CUdevice  );
void *_thcode(void *argument);
